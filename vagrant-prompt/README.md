This plugin prompts the status of the Vagrant VMs. It supports single-host and
multi-host configurations as well.

Look inside the source for documentation about custom variables.

## Based upon the work of Alberto Re <alberto.re@gmail.com>
