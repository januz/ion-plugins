## Autoenv

Provide helpers for Python's autoenv

### On source

When loading the plugin, autoenv will be started

### Function
|-----------------|-------------------------------|
| Function        | Description                   |
|-----------------|-------------------------------|
| `use_env <env>` | Use a new autoenv environment |
|-----------------|-------------------------------|
