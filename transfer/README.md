# `transfer` plugin

[`transfer.sh`](https://transfer.sh) is an easy to use file sharing service from the command line

Then you can:

- transfer a file:

```zsh
transfer file.txt
```

- transfer a whole directory (it will be automatically compressed):

```zsh
transfer directory/
```
